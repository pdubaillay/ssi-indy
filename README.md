# Exploring Aries Cloud Agent - Python

This project is about exploring a subset of capabilities provided by the Aries Cloud Agent (Python implementation). The focus is primarily set on issuing W3C credentials.

## How to play with it

### Setup

To play with the project, you need to:

1. Get [docker](https://www.docker.com/get-started/) on your machine
2. Run a [von-network](https://github.com/bcgov/von-network/blob/main/docs/UsingVONNetwork.md) locally

Once both are installed, set and export a variable named `VON_NETWORK_PATH` and that points to the location of your **von-network** installation folder.

### Run the project

In order to run, the project needs a running **von-network**, which can be started using the provided `scripts/ledger.[bash|ps1]` script: `./script/ledger.bash build` and then `./script/ledger.bash up`.

Once the **von-network** is running, you can start the project by issuing in your favorite terminal: `[sudo] docker-compose up -d`

### Dive in the web interfaces

Open your favorite web browser and visit:

- Cityhall controller: <http://localhost:8022/view>
- Alice controller: <http://localhost:8032/view>

Time to play !

## Supported workflows

### DIDs

Supported:

- Create a DID (sov or key)
- Publish a DID (partial)

Unsupported :

- Publish the first public DID of the controller
- Set DID as "Wallet public DID"

### Connections

Supported:

- Sequence "invite - receive-invitation - accept & send request - accept request"

Unsupported:

- Other sequences

### Credentials

Supported:

- Sequence "offer - accept & request - issue - store"
- `PermanentResident` credential

Unsupported :

- Other valid sequences (with negotiation for example)
- Custom W3C credential schema
- Indy schema and credentials

### Presentation proofs

Supported :

- Sending a validation request based on the `PermanentResident` schema

Unsupported :

- End of the workflow
- Custom validation rules
- Custom validation schema
- Indy format

## Developing

### Python environment

With `pip`:

- We recommend creating a virtual environment dedicated to the project
- `pip install -r requirements.txt`

With `poetry`:

- Install `poetry`
- `poetry install` while being in the `controller` folder containing the `pyproject.toml`

### Start the system

1. Start the ledger : `./scripts/ledger.bash up`
2. Update `docker-compose.yml` file to bind the admin port of the `aca-cityhall` agent to host's 8021 port
3. Start two agents and the Alice controller: `docker-compose up -d aca-cityhall aca-alice ctrl-alice`
4. Go in `controller/src` folder
5. Export variable: `FLASK_APP="controller.app:create_app()"`
6. Initialize database: `flask init-storage`
7. Run the development server: `flask run -p 8022 --reload`
