/*
 * Global functions.
 */

/**
 * Display the raw JSON data of a specific connection in a new tab.
 */
function displayRawConnectionInNewTab() {
    let hiddenInputText = document.getElementById("raw-conn");
    displayRawInNewTab(hiddenInputText.value);
}

/**
 * Display the raw JSON data of the credential in a new tab.
 * @param {str} id if of the credential
 */
function displayRawCredentialInNewTab(id) {
    fetch(document.location.origin + "/credentials/" + id).then(async (response) => {
        if (!response.ok) {
            throw new Error(await response.text());
        }
        displayRawInNewTab(await response.text());
    }).catch((error) => {
        displayAlert(error.message, "danger");
    });
}

/**
 * Display the raw JSON data of the proof in a new tab.
 * @param {str} id if of the proof
 */
function displayRawProofInNewTab(id) {
    fetch(document.location.origin + "/proofs/" + id).then(async (response) => {
        if (!response.ok) {
            throw new Error(await response.text());
        }
        displayRawInNewTab(await response.text());
    }).catch((error) => {
        displayAlert(error.message, "danger");
    });
}

/**
 * Display some raw data in a new tab
 * @param {str} raw raw data to display
 */
function displayRawInNewTab(raw) {
    let tab = window.open('about:blank', '_blank');
    tab.document.write(raw);
    tab.document.title = "Raw";
    tab.document.close();
}

/**
 * Create a new connection invitation by calling the backend.
 *
 * The invitation is displayed in #generatedInvitation.
 */
function createInvitation() {
    fetch(
        document.location.origin + "/connections",
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ "method": "create-invitation" })
        }
    ).then(async (response) => {
        if (!response.ok) {
            throw new Error(await response.text());
        }
        let txtArea = document.getElementById("generatedInvitation");
        txtArea.value = JSON.stringify(await response.json());
        toggleInvitationCreation();
    }).catch((error) => {
        console.log(error);
    });
}

/**
 * Read the content of #receivedInvitation and call the backend to accept
 * the invitation (and send a connection request to the other participant).
 * 
 * On success, the page is refreshed.
 */
function receiveInvitation() {
    let txtArea = document.getElementById("receivedInvitation");
    let txt = JSON.parse(txtArea.value).invitation;
    fetch(
        document.location.origin + "/connections",
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ "method": "receive-invitation", "invitation": txt })
        }
    ).then((response) => {
        if (!response.ok) {
            return response.text().then(txt => { throw new Error(txt); });
        }
        window.location.reload(true);
    }).catch((error) => {
        console.log(error);
    });
}

/**
 * Toggle the #generatedInvitation panel.
 */
function toggleInvitationCreation() {
    let txtAreaCreate = document.getElementById("generatedInvitation");
    txtAreaCreate.parentElement.classList.remove("d-none", "invisible");

    let txtAreaReceive = document.getElementById("receivedInvitation");
    txtAreaReceive.parentElement.classList.add("d-none", "invisible");
}

/**
 * Toggle the #receivedInvitation panel.
 */
function toggleInvitationReceive() {
    let txtAreaCreate = document.getElementById("generatedInvitation");
    txtAreaCreate.parentElement.classList.add("d-none", "invisible");

    let txtAreaReceive = document.getElementById("receivedInvitation");
    txtAreaReceive.parentElement.classList.remove("d-none", "invisible");
}

/**
 * Accept a connection request.
 * 
 * @param {str} connection_id the id of the connection
 */
function acceptConnectionRequest(connection_id) {
    fetch(
        document.location.origin + "/connections/" + connection_id,
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ "method": "accept-request" })
        }
    ).then((response) => {
        if (!response.ok) {
            return response.text().then(txt => { throw new Error(txt); });
        }
        window.location.reload(true);
    }).catch((error) => {
        displayAlert(error, "danger");
    });
}

/**
 * Accept a received connection invitation.
 * 
 * @param {str} connection_id the id of the connection
 */
function acceptInvitation(connection_id) {
    fetch(
        document.location.origin + "/connections/" + connection_id,
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ "method": "accept-invitation" })
        }
    ).then((response) => {
        if (!response.ok) {
            return response.text().then(txt => { throw new Error(txt); });
        }
        window.location.reload(true);
    }).catch((error) => {
        displayAlert(error, "danger");
    });
}

/**
 * Send a message through the provided connection.
 * The message's content is picked-up in the #msgContent textarea.
 * 
 * @param {str} connection_id the id of the connection
 */
function sendMessage(connection_id) {
    let txtArea = document.getElementById("msgContent");
    let txt = {
        content: txtArea.value
    };

    fetch(
        document.location.origin + "/connections/" + connection_id + "/messages",
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(txt)
        }
    ).then((response) => {
        if (!response.ok) {
            return response.text().then(txt => { throw new Error(txt); });
        }
        window.location.reload(true);
    }).catch((error) => {
        console.log(error);
    });
}

/**
 * Publish the given DID to the ledger.
 * 
 * @param {str} did the did to publish
 */
function publishDID(did) {
    fetch(
        document.location.origin + "/dids/" + did,
        {
            method: "POST",
            headers: { "Content-Type": "application/json" }
        }
    ).then((response) => {
        if (!response.ok) {
            return response.text().then(txt => { throw new Error(txt); });
        }
        window.location.reload(true);
    }).catch((error) => {
        displayAlert(error, "danger");
    });
}

/**
 * Display a dismissable alert on the page.
 * 
 * @param {str} message the message to display
 * @param {str} type danger, warning, success, primary, secondary (bs classes)
 */
function displayAlert(message, type) {
    const alertPlaceholder = document.getElementById("alertPlaceholder");
    const wrapper = document.createElement("div");
    wrapper.innerHTML = [
        `<div class="alert alert-${type} alert-dismissible" role="alert">`,
        `   <div>${message}</div>`,
        '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
        '</div>'
    ].join('')

    alertPlaceholder.append(wrapper);
}

/**
 * Create a new DID.
 * 
 * @param {str} method key or sovrin
 * @param {str} key_type ed25519 or bls12381g2
 */
function createDID(method, key_type) {
    const errorPlaceholder = document.getElementById("modalErrorPlaceHolder");
    fetch(
        document.location.origin + "/dids",
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ "method": method, "options": { "key_type": key_type } })
        }
    ).then((response) => {
        if (!response.ok) {
            return response.text().then(txt => { throw new Error(txt); });
        }
        window.location.reload(true);
    }).catch((error) => {
        const wrapper = document.createElement("div");
        wrapper.innerHTML = [
            `<div class="alert alert-danger alert-dismissible" role="alert">`,
            `   <div>${error.message}</div>`,
            '   <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>',
            '</div>'
        ].join('')
        errorPlaceholder.append(wrapper);
    });
}

/**
 * Send a credential offer using the information filled in the form.
 * 
 * @param {HTMLFormControlsCollection} formElements form elements
 */
function sendCredentialOffer(formElements) {
    cred = {
        "connection_id": formElements["submitCredBtn"].attributes["data-conn-id"].value,
        "issuerDID": formElements["issuerDIDInput"].value,
        "givenName": formElements["givenNameInput"].value,
        "familyName": formElements["familyNameInput"].value,
        "birthCountry": formElements["birthCountryInput"].value,
        "birthDate": formElements["birthDateInput"].value,
        "holderDID": formElements["holderDIDInput"].value
    };

    fetch(
        document.location.origin + "/credentials",
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(cred)
        }
    ).then((response) => {
        if (!response.ok) {
            return response.text().then(txt => { throw new Error(txt); });
        }
        window.location.reload(true);
    }).catch((error) => {
        console.log(error);
    });
}

/**
 * Issue a credential (Issuer) to the Holder.
 * 
 * @param {str} cred_id id of the credential
 */
function issueCredential(cred_id) {
    fetch(
        document.location.origin + "/credentials/" + cred_id,
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ "method": "issue" })
        }
    ).then((response) => {
        if (!response.ok) {
            return response.text().then(txt => { throw new Error(txt); });
        }
        window.location.reload(true);
    }).catch((error) => {
        displayAlert(error, "danger");
    });
}

/**
 * Accept a credential offer and send a request to the Issuer.
 * 
 * @param {str} cred_id id of the credential
 */
function requestCredential(cred_id) {
    fetch(
        document.location.origin + "/credentials/" + cred_id,
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ "method": "send-request" })
        }
    ).then((response) => {
        if (!response.ok) {
            return response.text().then(txt => { throw new Error(txt); });
        }
        window.location.reload(true);
    }).catch((error) => {
        displayAlert(error, "danger");
    });
}

/**
 * Store a received credential in the cloud wallet.
 * 
 * @param {str} cred_id id of the credential
 */
function storeCredential(cred_id) {
    fetch(
        document.location.origin + "/credentials/" + cred_id,
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ "method": "store" })
        }
    ).then((response) => {
        if (!response.ok) {
            return response.text().then(txt => { throw new Error(txt); });
        }
        window.location.reload(true);
    }).catch((error) => {
        displayAlert(error, "danger");
    });
}

/**
 * Send a proof request using the information filled in the form.
 * 
 * @param {HTMLFormControlsCollection} formElements form elements
 */
function sendProofRequest(formElements) {
    proof = {
        "connection_id": formElements["submitProofReqBtn"].attributes["data-conn-id"].value,
        "fields": []
    }

    if (!formElements["givenNameInput"].hasAttribute("disabled")) {
        field = {
            path: ["$.credentialSubject.givenName"]
        };
        if (formElements["givenNameInput"].value != "") {
            field["filter"] = { "const": formElements["givenNameInput"].value };
        }
        proof["fields"].push(field);
    }

    if (!formElements["familyNameInput"].hasAttribute("disabled")) {
        field = {
            path: ["$.credentialSubject.familyName"]
        };
        if (formElements["familyNameInput"].value != "") {
            field["filter"] = { "const": formElements["familyNameInput"].value };
        }
        proof["fields"].push(field);
    }

    if (!formElements["birthCountryInput"].hasAttribute("disabled")) {
        field = {
            path: ["$.credentialSubject.birthCountry"]
        };
        if (formElements["birthCountryInput"].value != "") {
            field["filter"] = { "const": formElements["birthCountryInput"].value };
        }
        proof["fields"].push(field);
    }

    if (!formElements["birthDateInput"].hasAttribute("disabled")) {
        field = {
            path: ["$.credentialSubject.birthDate"]
        };
        if (formElements["birthDateInput"].value != "") {
            field["filter"] = { "const": formElements["birthDateInput"].value };
        }
        proof["fields"].push(field);
    }

    fetch(
        document.location.origin + "/proofs",
        {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(proof)
        }
    ).then((response) => {
        if (!response.ok) {
            return response.text().then(txt => { throw new Error(txt); });
        }
        window.location.reload(true);
    }).catch((error) => {
        console.log(error);
    });
};

/*
 * Page manipulation
 */
window.onload = function () {
    // When the user clicks on a row in the connections table, display connection information
    let tbodyConnections = document.getElementById("connections")
    tbodyConnections.childNodes.forEach(node => {
        node.addEventListener("click", function (e) {
            document.location = document.location.origin + "/view/connections/" + node.id;
        })
    });

    // Reload the page when the "create connection" modal is closed
    let closeModalBtn = document.getElementById("closeConnModalBtn");
    closeModalBtn.addEventListener("click", function (e) {
        document.location.reload(true);
    });

    // Intercept submit event raised by the DID creation form and call the appropriate JS function
    let didForm = document.getElementById("didForm");
    didForm.addEventListener("submit", function (e) {
        e.preventDefault();
        createDID(didForm.elements["inputSelectMethod"].value, didForm.elements["inputSelectKeyType"].value);
    });

    // Same for credentials, but only if the form exists on the page
    let credentialForm = document.getElementById("credentialForm");
    if (credentialForm != null) {  // Credentials only on the connection template
        credentialForm.addEventListener("submit", function (e) {
            e.preventDefault();
            sendCredentialOffer(credentialForm.elements);
        });
    }

    // Same for proofs
    let proofForm = document.getElementById("proofForm");
    if (proofForm != null) {  // Credentials only on the connection template
        proofForm.addEventListener("submit", function (e) {
            e.preventDefault();
            sendProofRequest(proofForm.elements);
        });
    }

    // Make each checkboxe in #sendProofRe control its input
    document.querySelectorAll("#sendProofReq .form-check-input").forEach((e) => {
        e.addEventListener("click", function (evt) {
            if (e.checked) {
                e.parentNode.parentNode.querySelector("[disabled]").removeAttribute("disabled");
            } else {
                e.parentNode.parentNode.querySelector("input[type='text'], input[type='date']").setAttribute("disabled", true);
            }
        });
    });
}