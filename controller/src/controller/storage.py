"""
This module contains classes and functions related to the application's
storage.
"""
from __future__ import annotations

import sqlite3
import json
from typing import Any, Dict, Iterable, List, Tuple

import click
from flask import Flask, g, current_app
from flask.cli import with_appcontext


class AppStorage:
    """
    Represent the application's storage.

    This object proxies calls to the underlaying database to ease interactions
    with it.
    """

    def __init__(self, db_url: str) -> None:
        """
        Create a new :class:`AppStorage` object.

        :param db_url: URL to reach the database
        """
        self._db = sqlite3.connect(db_url)
        self._db.row_factory = sqlite3.Row

    def put_connection(self, conn: Dict[str, Any]) -> None:
        """
        Put the provided connection into the storage.

        If a connection with the same connection id exists, it is replaced by
        the provided one. The `conn` dict must contain the following fields:

        - connection_id
        - state
        - created_at
        - rfc23_state
        - updated_at

        The entire `conn` dict is also stored as raw JSON data.

        :param conn: the connection information
        """
        c = self._db.cursor()
        c.execute(
            "INSERT OR REPLACE INTO Connections VALUES (?, ?, ?, ?, ?, ?)",
            (
                conn["connection_id"],
                conn["state"],
                conn["created_at"],
                conn["rfc23_state"],
                json.dumps(conn),
                conn["updated_at"],
            ),
        )
        self._db.commit()

    def put_connections(self, conns: Iterable[Dict[str, Any]]) -> None:
        """
        Put multiple connections in the database, see
        :meth:`AppStorage.put_connection`.

        :param conns: a set of connections to put in the storage
        """
        for conn in conns:
            self.put_connection(conn)

    def get_connection(self, id: str) -> None | Dict[str, Any]:
        """
        Retrieve a connection given it's ID.

        :param id: id of the connection
        :return: either the connection's data of `None` if there is no such
            connection in the application's storage
        """
        c = self._db.cursor()
        records = [
            r for r in c.execute("SELECT RAW FROM Connections WHERE ID=?", (id,))
        ]
        return json.loads(records[0][0]) if len(records) == 1 else None

    def get_connections(self) -> List[Tuple[str, ...]]:
        """
        Retrieve all connections store in the application's storage.

        :return: a list of tuples, each one containing five fields: id, state,
            rfc23-state, creation date, last update.
        """
        c = self._db.cursor()
        records = [
            r
            for r in c.execute(
                "SELECT ID, STATE, `RFC23-STATE`, `CREATED-AT`, `UPDATED-AT` FROM Connections"
            )
        ]
        return records

    def add_message(self, msg: Dict[str, Any]) -> None:
        """
        Add the provided message in the application's storage.

        The `msg` dict must contain the following fields:

        - message_id (not necessary unique)
        - connection_id (must match a stored connection's id)
        - content
        - state

        :param msg: the message to store
        """
        c = self._db.cursor()
        c.execute(
            "INSERT INTO Messages(M_ID, C_ID, CONTENT, STATE) VALUES (?,?,?,?)",
            (msg["message_id"], msg["connection_id"], msg["content"], msg["state"]),
        )
        self._db.commit()

    def get_messages(self, id: str) -> List[Tuple[str, ...]]:
        """
        Retrieve all stored messages for the provided connection ID.

        :param id: id of the connection
        :return: a list of tuples, each one having the following fields: m_id,
            sate, content
        """
        c = self._db.cursor()
        records = [
            r
            for r in c.execute(
                "SELECT M_ID, STATE, CONTENT FROM Messages WHERE C_ID=?", (id,)
            )
        ]
        return records

    def put_dids(self, dids: Iterable[Dict[str, Any]]) -> None:
        """
        Put multiple DIDs in the application's storage.

        If some provided DIDs have the same ID, then stored ones are replaced.
        Each provided DID is a dict with the following keys:

        - did
        - verkey
        - posture
        - key_type
        - method

        :param dids: a set of dids to add
        """
        c = self._db.cursor()
        c.executemany(
            "INSERT OR REPLACE INTO Dids VALUES (?, ?, ?, ?, ?)",
            (
                (
                    did["did"],
                    did["verkey"],
                    did["posture"],
                    did["key_type"],
                    did["method"],
                )
                for did in dids
            ),
        )
        self._db.commit()

    def get_dids(self) -> List[Tuple[str, ...]]:
        """
        Retrieve all stored DIDs.

        :return: a list of tuples, each one having the following fields: did,
            verkey, posture, key_type and method.
        """
        c = self._db.cursor()
        records = [r for r in c.execute("SELECT * FROM Dids")]
        return records

    def get_did(self, did: str) -> Tuple[str, ...] | None:
        """
        Retrieve a specific DID using it's ID.

        :param did: the identifier of the desirated DID
        :return: either a tuple (did, verkey, posture, key_type, method) or
            `None`
        """
        c = self._db.cursor()
        records = [r for r in c.execute("SELECT * FROM Dids WHERE DID=?", (did,))]
        return records[0] if len(records) == 1 else None

    def put_credential(self, cred: Dict[str, Any]) -> None:
        """
        Put a credential in the application storage.

        The `cred` dict must contain the following keys:

        - cred_ex_id
        - connection_id (which must match the id of a stored connection)
        - state
        - created_at
        - updated_at

        The entire `cred` object is dumped in the storage as JSON data.

        :param cred: the credential to put
        """
        c = self._db.cursor()
        c.execute(
            "INSERT OR REPLACE INTO Credentials VALUES (?, ?, ?, ?, ?, ?)",
            (
                cred["cred_ex_id"],
                cred["connection_id"],
                cred["state"],
                cred["created_at"],
                cred["updated_at"],
                json.dumps(cred),
            ),
        )
        self._db.commit()

    def put_credentials(self, creds: Iterable[Dict[str, Any]]) -> None:
        """
        Put a bunch of credentials in the application storage, see
        :meth:`AppStorage.put_credential`.

        :param creds: a bunch of credentials
        """
        for cred in creds:
            self.put_credential(cred)

    def get_credential(self, id: str) -> None | Dict[str, Any]:
        """
        Retrieve a credential using its ID.

        :param id: the ID of the credential
        :return: the credential data or `None` if there is no such credential
            stored
        """
        c = self._db.cursor()
        records = [
            r for r in c.execute("SELECT RAW FROM Credentials WHERE ID=?", (id,))
        ]
        return json.loads(records[0][0]) if len(records) == 1 else None

    def get_credentials(self, conn_id: str | None = None) -> List[Tuple[str, ...]]:
        """
        Get all credentials.

        :param conn_id: if not `None`, filter credentials to only get those
            related to a specific connection.
        :return: a list of tuples (id, state, creation date, last update)
        """
        c = self._db.cursor()
        req, req_args = "SELECT ID, STATE, CREATED_AT, UPDATED_AT FROM Credentials", ()
        if conn_id is not None:
            req, req_args = (
                "SELECT ID, STATE, CREATED_AT, UPDATED_AT FROM Credentials WHERE C_ID=?",
                (conn_id,),
            )

        records = [r for r in c.execute(req, req_args)]
        return records

    def put_proof_presentation(self, proof: Dict[str, Any]) -> None:
        """
        Put a proof presentation in the application storage.

        The `proof` dict must contain the following keys:

        - pres_ex_id
        - connection_id (which must match the id of a stored connection)
        - state
        - created_at
        - updated_at

        The entire `proof` object is dumped in the storage as JSON data.

        :param proof: the proof presentation to put
        """
        c = self._db.cursor()
        c.execute(
            "INSERT OR REPLACE INTO Proofs VALUES (?, ?, ?, ?, ?, ?)",
            (
                proof["pres_ex_id"],
                proof["connection_id"],
                proof["state"],
                proof["created_at"],
                proof["updated_at"],
                json.dumps(proof),
            ),
        )
        self._db.commit()

    def put_proof_presentations(self, proofs: Iterable[Dict[str, Any]]) -> None:
        """
        Put a bunch of proof presentations in the application storage, see
        :meth:`AppStorage.put_proof_presentation`.

        :param proofs: a bunch of proof presentations
        """
        for proof in proofs:
            self.put_proof_presentation(proof)

    def get_proof_presentation(self, id: str) -> None | Dict[str, Any]:
        """
        Retrieve a proof presentation using its ID.

        :param id: the ID of the proof presentation
        :return: the proof presentation data or `None` if there is no such
            proof presentation stored
        """
        c = self._db.cursor()
        records = [r for r in c.execute("SELECT RAW FROM Proofs WHERE ID=?", (id,))]
        return json.loads(records[0][0]) if len(records) == 1 else None

    def get_proof_presentations(
        self, conn_id: str | None = None
    ) -> List[Tuple[str, ...]]:
        """
        Get all proof presentations.

        :param conn_id: if not `None`, filter proof presentations to only get those
            related to a specific connection.
        :return: a list of tuples (id, state, creation date, last update)
        """
        c = self._db.cursor()
        req, req_args = "SELECT ID, STATE, CREATED_AT, UPDATED_AT FROM Proofs", ()
        if conn_id is not None:
            req, req_args = (
                "SELECT ID, STATE, CREATED_AT, UPDATED_AT FROM Proofs WHERE C_ID=?",
                (conn_id,),
            )

        records = [r for r in c.execute(req, req_args)]
        return records

    def close(self) -> None:
        """Close the storage for good."""
        self._db.close()


def get_storage() -> AppStorage:
    """
    Retrieve from the application globals the  application storage.

    :return: the application storage
    """
    if "_storage" not in g:
        g._storage = AppStorage(current_app.config["DATABASE_PATH"])
    return g._storage


def close_storage(e=None):
    """
    Close the application storage and remove it from application globals.

    :param e: unused
    """
    storage: AppStorage = g.pop("_storage", None)
    if storage is not None:
        storage.close()


def init_storage() -> None:
    """Initialize the application storage (clean and rebuild tables)."""
    storage = get_storage()
    with current_app.open_resource(current_app.config["SCHEMA_PATH"]) as f:
        storage._db.executescript(f.read().decode("utf-8"))


@click.command("init-storage")
@with_appcontext
def init_storage_command() -> None:
    """
    Click command to initialize the application storage.

    Usage: flask init-storage
    """
    init_storage()
    click.echo("Initialized application storage.")


def init_app(app: Flask) -> None:
    """
    Initialize the application.

    Register the storage closing as a teardown action and the init-storage
    command in click.

    :param app: the application
    """
    app.teardown_appcontext(close_storage)
    app.cli.add_command(init_storage_command)
