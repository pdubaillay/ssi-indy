"""This module contains the factory that creates the application's instance."""
from __future__ import annotations

import requests
from flask import Flask

from controller.storage import init_app, AppStorage, get_storage
from controller.routes import (
    viewsBP,
    connectionsBP,
    credentialsBP,
    topicsBP,
    didsBP,
    proofsBP,
)


def create_app() -> Flask:
    """
    Factory function that creates the application and configures it.

    A default configuration is first loaded:

    - DATABASE_PATH: db.sqlite
    - SCHEMA_PATH: schema.sql
    - NAME: Default controller
    - ACA_ADMIN_URL: http://localhost:8021

    Then the application will check environment variables and override default
    values if necessary. To override an entry, export an environment variable
    prefixed with `CONTROLLER_`. For example, to override the database name
    you can export the following variable:
    `CONTROLLER_DATABASE_PATH="alice.sqlite"`.

    :return: the application instance well configured
    """
    app = Flask(__name__, instance_relative_config=True)

    # Configuration
    app.config.from_mapping(
        {
            "DATABASE_PATH": "db.sqlite",
            "SCHEMA_PATH": "schema.sql",
            "NAME": "Default controller",
            "ACA_ADMIN_URL": "http://localhost:8021",
        }
    )
    app.config.from_prefixed_env(
        prefix="CONTROLLER"
    )  # Override with environment variables

    # Application initialization
    init_app(app)

    @app.before_first_request
    def populate_store():
        """Prepopulate the application store with existing connections and credentials."""
        storage: AppStorage = get_storage()

        # Retrieve existing connections and store them in the application storage
        connections = requests.get(f"{app.config['ACA_ADMIN_URL']}/connections").json()[
            "results"
        ]
        storage.put_connections(connections)

        # Retrieve existing credentials and store them in the application storage
        credentials = requests.get(
            f"{app.config['ACA_ADMIN_URL']}/issue-credential-2.0/records"
        ).json()["results"]
        storage.put_credentials([cred["cred_ex_record"] for cred in credentials])

        # Retrieve existing proofs and store them in the application storage
        proofs = requests.get(
            f"{app.config['ACA_ADMIN_URL']}/present-proof-2.0/records"
        ).json()["results"]
        storage.put_proof_presentations(proofs)

    @app.before_request
    def query_dids():
        """
        Retrieve existing DIDs and store them in our database.

        Since there is no webhook notifying us about the creation or the deletion
        of a DID, we have to query them each time a request is made ...
        """
        storage: AppStorage = get_storage()
        dids = requests.get(f"{app.config['ACA_ADMIN_URL']}/wallet/did").json()[
            "results"
        ]
        storage.put_dids(dids)

    # Register Blueprints
    app.register_blueprint(viewsBP)
    app.register_blueprint(connectionsBP)
    app.register_blueprint(credentialsBP)
    app.register_blueprint(topicsBP)
    app.register_blueprint(didsBP)
    app.register_blueprint(proofsBP)

    return app
