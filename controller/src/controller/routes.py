"""This module contains the set of routes the application declares."""
from __future__ import annotations

import json
from pprint import pprint
from typing import Any, Tuple, Dict
from datetime import datetime
from uuid import uuid4
import requests
from flask import current_app, request, render_template, Blueprint

from controller.storage import AppStorage, get_storage


# Declare the blueprints used by the application
viewsBP = Blueprint("view", __name__, url_prefix="/view")
connectionsBP = Blueprint("connections", __name__, url_prefix="/connections")
credentialsBP = Blueprint("credentials", __name__, url_prefix="/credentials")
topicsBP = Blueprint("topics", __name__, url_prefix="/topic")
didsBP = Blueprint("dids", __name__, url_prefix="/dids")
proofsBP = Blueprint("proofs", __name__, url_prefix="/proofs")


@viewsBP.context_processor
def inject_connections():
    """Inject the list of connections in the template's context."""
    storage: AppStorage = get_storage()
    return dict(connections=storage.get_connections())


@viewsBP.context_processor
def inject_dids():
    """Inject the list of DIDs in the template's context."""
    storage: AppStorage = get_storage()
    return dict(dids=storage.get_dids())


@viewsBP.context_processor
def inject_controller_name():
    """Inject the controller's name in the template's context."""
    return dict(name=current_app.config["NAME"])


@viewsBP.route("/")
def index() -> str:
    """
    Render the main page listing all available connections.

    :return: the html page
    """
    data = {
        "connection": None,
    }
    return render_template("main.html.jinja", **data)


@viewsBP.route("/connections/<id>")
def display_connection(id: str) -> str:
    """
    Render a page displaying information related to a specific connection.

    :return: the html page
    """
    storage: AppStorage = get_storage()
    connection = storage.get_connection(id)
    data = {
        "connection": connection,
        "messages": storage.get_messages(id),
        "raw": json.dumps(connection),
        "credentials": storage.get_credentials(id),
        "proofs": storage.get_proof_presentations(id),
    }
    return render_template("connection.html.jinja", **data)


@topicsBP.route("/connections/", methods=["POST"])
def update_connection() -> Tuple[Dict, int]:
    """
    Update a connection using the provided information.

    The request's body must contain:

    - connection_id
    - state
    - rfc23_state
    - create_at
    - updated_at

    :return: 200 with empty body if everything goes well
    """
    content: Dict[str, Any] = request.json  # type: ignore
    get_storage().put_connection(content)
    return {}, 200


@topicsBP.route("/basicmessages/", methods=["POST"])
def new_basic_message() -> Tuple[Dict, int]:
    """
    Add a new basic message in the application's storage.

    The request's body must contain:

    - message_id
    - connection_id
    - content
    - state

    :return: 200 with empty body if everything goes well
    """
    data: Dict[str, Any] = request.json  # type: ignore
    get_storage().add_message(data)
    return {}, 200


@topicsBP.route("/issue_credential_v2_0/", methods=["POST"])
def update_credential() -> Tuple[Dict, int]:
    """
    Update a credential record with the provided information.

    The request's body must contain:

    - cred_ex_id
    - connection_id
    - state
    - created_at
    - updated_at

    :return: 200 with empty body if everything goes well
    """
    data: Dict[str, Any] = request.json  # type: ignore
    get_storage().put_credential(data)
    return {}, 200


@topicsBP.route("/present_proof_v2_0/", methods=["POST"])
def update_presentation_proof() -> Tuple[Dict, int]:
    """
    Update a presentation proof record with the provided information.

    The request's body must contain:

    - pres_ex_id
    - connection_id
    - state
    - created_at
    - updated_at

    :return: 200 with empty body if everything goes well
    """
    data: Dict[str, Any] = request.json  # type: ignore
    get_storage().put_proof_presentation(data)
    return {}, 200


@connectionsBP.route("/", methods=["POST"])
def create_connection():
    """
    Create a new connection.

    There are two ways to create a connection : either by creating an
    invitation or by receiving one.

    If the creation method is by creating an invitation, the body must be::

    {
        method: "create-invitation"
    }

    If the creation method is by receiving an invitation, the body must be::

    {
        method: "receive-invitation",
        invitation: {
            ...
        }
    }

    The `invitation` field must be a JSON object representing the invitation.

    :return: 200 with the created invitation if method is `create-invitation`,
        200 with an empty body otherwise

    """
    data: Dict[str, Any] = request.json  # type: ignore
    if data["method"] == "create-invitation":
        response = requests.post(
            f"{current_app.config['ACA_ADMIN_URL']}/connections/create-invitation",
            headers={"Content-Type": "application/json"},
        )
    else:
        response = requests.post(
            f"{current_app.config['ACA_ADMIN_URL']}/connections/receive-invitation",
            headers={"Content-Type": "application/json"},
            json=data["invitation"],
        )
    return response.text, response.status_code


@connectionsBP.route("/<id>", methods=["POST"])
def accept_connection(id: str) -> Tuple[Any, int]:
    """
    Accept the connection.

    There are two ways to accept a connection:

    1. Accept an incoming connection request.
    2. Accept an incoming connection invitation (which will send a connection
        request to the other participant).

    The request's body must be the following::

    {
        method: "[chosen-method]"
    }

    with `[chosen-method]` being either `accept-request` or `accept-invitation`.

    :param id: the connection's id
    :return: 200 with an empty body
    """
    data: Dict[str, Any] = request.json  # type: ignore
    if data["method"] == "accept-request":
        response = requests.post(
            f"{current_app.config['ACA_ADMIN_URL']}/connections/{id}/accept-request",
            headers={"Content-Type": "application/json"},
        )
    else:
        response = requests.post(
            f"{current_app.config['ACA_ADMIN_URL']}/connections/{id}/accept-invitation",
            headers={"Content-Type": "application/json"},
        )
    return response.text, response.status_code


@connectionsBP.route("/<id>/messages", methods=["POST"])
def send_message(id) -> Tuple[Any, int]:
    """
    Send a message through the given connection.

    The request's body must be the following::

    {
        content: "[message content]"
    }

    :param id: the connection's id
    :return: 200 with an empty body
    """
    data: Dict[str, Any] = request.json  # type: ignore
    response = requests.post(
        f"{current_app.config['ACA_ADMIN_URL']}/connections/{id}/send-message",
        headers={"Content-Type": "application/json"},
        json=data,
    )

    if response.status_code == 200:
        get_storage().add_message(
            {
                "message_id": "",
                "connection_id": id,
                "content": data["content"],
                "state": "sent",
            }
        )
    return response.text, response.status_code


@didsBP.route("/", methods=["POST"])
def create_did() -> Tuple[Any, int]:
    """
    Create a new DID.

    Delegate the DID creation to the cloud wallet.
    """
    data: Dict[str, Any] = request.json  # type: ignore
    response = requests.post(
        f"{current_app.config['ACA_ADMIN_URL']}/wallet/did/create",
        headers={"Content-Type": "application/json"},
        json=data,
    )
    return response.text, response.status_code


@didsBP.route("/<id>", methods=["POST"])
def publish_did(id: str) -> Tuple[Any, int]:
    """
    Publish the given DID to the ledger.

    Delegate the work to the cloud wallet.

    :param id: the DID
    """
    did = get_storage().get_did(id)
    response = requests.post(
        f"{current_app.config['ACA_ADMIN_URL']}/ledger/register-nym",
        params={"did": id, "verkey": did["verkey"]},
        headers={"Content-Type": "application/json"},
    )
    return response.text, response.status_code


@credentialsBP.route("/", methods=["POST"])
def send_credential_offer() -> Tuple[Any, int]:
    """
    Send a credential offer.

    The credential is fixed (PermanentResident) and built using the provided
    information. The request's body must contain:

    - connection_id
    - issuerDID
    - givenName
    - familyName
    - birthCountry
    - birthDate
    - holderDID

    :return: 200 with empty body
    """
    data: Dict[str, Any] = request.json  # type: ignore
    issuerDID = get_storage().get_did(data["issuerDID"])
    connection = get_storage().get_connection(data["connection_id"])

    proofType = "BbsBlsSignature2020"
    if issuerDID[3] == "ed25519":
        proofType = "Ed25519Signature2018"

    holderDID: str = data.get("holderDID", None)
    if holderDID is None:
        holderDID = connection["their_did"]

    if not holderDID.startswith("did"):
        holderDID = "did:sov:" + holderDID

    if issuerDID[4] == "sov":
        issuerDID = "did:sov:" + issuerDID[0]
    else:
        issuerDID = issuerDID[0]

    cred = {
        "connection_id": data["connection_id"],
        "filter": {
            "ld_proof": {
                "credential": {
                    "@context": [
                        "https://www.w3.org/2018/credentials/v1",
                        "https://w3id.org/citizenship/v1",
                    ],
                    "type": ["VerifiableCredential", "PermanentResident"],
                    "id": f"https://credential.example.com/residents/{str(uuid4())}",
                    "issuer": issuerDID,
                    "issuanceDate": datetime.now().isoformat(),
                    "credentialSubject": {
                        "type": ["PermanentResident"],
                        "id": holderDID,
                        "givenName": data["givenName"],
                        "familyName": data["familyName"],
                        "birthCountry": data["birthCountry"],
                        "birthDate": data["birthDate"],
                    },
                },
                "options": {"proofType": proofType},
            }
        },
    }

    response = requests.post(
        f"{current_app.config['ACA_ADMIN_URL']}/issue-credential-2.0/send-offer",
        headers={"Content-Type": "application/json"},
        json=cred,
    )
    if response.status_code != 200:
        return response.text, response.status_code
    return {}, 200


@credentialsBP.route("/<id>", methods=["POST"])
def manage_credential(id: str) -> Tuple[Any, int]:
    """
    Effect an operation on a credential.

    Three operations are available:

    1. send-request
    2. issue
    3. store

    The request's body must be the following::

    {
        method: "[chosen-operation]"
    }

    :return: 200 with an empty body
    """
    data: Dict[str, Any] = request.json  # type: ignore
    if data["method"] == "issue":
        response = requests.post(
            f"{current_app.config['ACA_ADMIN_URL']}/issue-credential-2.0/records/{id}/issue",
            headers={"Content-Type": "application/json"},
            json={"comment": f"Issued by {current_app.config['NAME']}"},
        )
    elif data["method"] == "send-request":
        response = requests.post(
            f"{current_app.config['ACA_ADMIN_URL']}/issue-credential-2.0/records/{id}/send-request",
            headers={"Content-Type": "application/json"},
        )
    else:
        response = requests.post(
            f"{current_app.config['ACA_ADMIN_URL']}/issue-credential-2.0/records/{id}/store",
            headers={"Content-Type": "application/json"},
        )
    return response.text, response.status_code


@credentialsBP.route("/<id>", methods=["GET"])
def get_credential(id: str) -> Tuple[Any, int]:
    """
    Retrieve a credential raw data from its ID.

    :param id: id of the credential
    :return: 200 with JSON body
    """
    appStorage: AppStorage = get_storage()
    return appStorage.get_credential(id), 200


@proofsBP.route("/", methods=["POST"])
def request_proof() -> Tuple[Any, int]:
    data: Dict[str, Any] = request.json  # type: ignore
    proof_req: Dict[str, Any] = {
        "comment": f"Created by {current_app.config['NAME']}",
        "connection_id": data["connection_id"],
        "presentation_request": {
            "dif": {
                "presentation_definition": {
                    "id": str(uuid4()),
                    "format": {
                        "ldp_vp": {
                            "proof_type": [
                                "Ed25519Signature2018",
                                "BbsBlsSignature2020",
                            ]
                        }
                    },
                    "input_descriptors": [
                        {
                            "id": "permanent_resident_input",
                            "name": "Permant resident data",
                            "schema": [
                                {
                                    "uri": "https://www.w3.org/2018/credentials#VerifiableCredential"
                                },
                                {
                                    "uri": "https://w3id.org/citizenship#PermanentResident"
                                },
                            ],
                            "constraints": {
                                "limit_disclosure": "required",
                                "fields": data["fields"],
                            },
                        }
                    ],
                },
            }
        },
    }
    response = requests.post(
        f"{current_app.config['ACA_ADMIN_URL']}/present-proof-2.0/send-request",
        headers={"Content-Type": "application/json"},
        json=proof_req,
    )
    return response.text, response.status_code


@proofsBP.route("/<id>", methods=["GET"])
def get_presentation_proof(id: str) -> Tuple[Any, int]:
    """
    Retrieve a presentation proof raw data from its ID.

    :param id: id of the presentation proof
    :return: 200 with JSON body
    """
    appStorage: AppStorage = get_storage()
    return appStorage.get_proof_presentation(id), 200
